## Contents:

- src: python scripts
  - problem_1.py
  - problem_2 .py
- data: important csv files that result from the implementation of Problem 1 
  - consumption-aggregated -> csv with the estimated aggregated consumption values (necessary for clustering)
  - consumption-without_NaN-BayesianRegressor: csv with the Bayesian predictions of the missing values (consumption data)
- model: .h5 lstm model from Problem 1 implementation
- report: pdf of the general overview of the implementation

