# Author: Christina Tzogka
# Created: 17.09.2021

import copy
import pandas as pd
import numpy as np
from random import randint
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR, LinearSVR
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense
from keras.models import load_model
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.impute import SimpleImputer
from time import time
import os.path

# ~~~~~~~~~~~~~~~~~~~~~~~ 1. Problem: Energy Prediction ~~~~~~~~~~~~~~~~~~~~~~~~
# A. aggregated monthly and yearly energy consumption per installation
# B. aggregated daily energy for all installation
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Read data
dir = "/home/christina/evaluation_test"
consumption_path = os.path.join(dir, "data/consumption.csv")
consumption_data = pd.read_csv(consumption_path)

# Estimate the num of missing values per column
# and missing values percentage in the whole dataset
total_num_of_nan = np.sum(consumption_data.isnull().sum().values)
perc_of_nan = total_num_of_nan * 100 / (consumption_data.shape[0] * consumption_data.shape[1])
print()
print("*" * 75)
print("\t\t\t\t\t\tDATASET REPORT:")
print("The total number of NaN values in the consumption data is: ", total_num_of_nan)
print(f"The percentage of NaN values in the consumption data is: {np.round(perc_of_nan, 2)}%")
print("*" * 75)

# ~~~~~~~~~~~~~~~~~~~~~~~ STEP #1: Impute missing values ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Store measurements per installation in array, in order to replace the missing values
array = consumption_data.iloc[:, 1:]

# Fill missing values with SimpleImputer (mean per column)
consumption_data_impute_missing = copy.deepcopy(array)
simple_imputer = SimpleImputer(strategy='mean')
updated_consumption_data_impute_missing = simple_imputer.fit_transform(consumption_data_impute_missing)
# Check if there is any NaN value in the updated dataset
# print(np.any(np.isnan(updated_consumption_data_impute_missing)))

# ~~~~~~~~~~~~ STEP #2: split the dataset into training data (x) and target data (y) ~~~~~~~~~~~~~~~
# The last column (low num of NaN values) is supposed to be the target data (y),
# while the rest of columns correspond to the training data (x)
y = updated_consumption_data_impute_missing[:, -1]
x = updated_consumption_data_impute_missing[:, :-1]
# Splitting to train and test data by making use of the random state,
# in order to control the shuffling (same set on different executions in the system)
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.1, random_state=4001)

# ~~~~~~~~~~~~~~~~~~ STEP #3a: Train/Evaluate the regression model ~~~~~~~~~~~~~~~~~~~~~~~~
# Run a model comparison in order to choose the optimal model
# regressor = KNeighborsRegressor()
# regressor = LinearRegression()
# regressor = LinearSVR()
# regressor = SVR()
regressor = linear_model.BayesianRidge()
regressor.fit(x_train, y_train)
pred_ML = regressor.predict(x_test)
# Evaluate the regressor by estimating the R2/MSE/MAE scores
print()
print("*" * 75)
print("\t\t\tML REGRESSION MODEL EVALUATION:")
print("Mean Squared Error (MSE) = ", mean_squared_error(y_test, pred_ML))
print("Mean Absolute Error (MAE) = ", mean_absolute_error(y_test, pred_ML))
print("R2 = ", r2_score(y_test, pred_ML))
print("*" * 75)

# Visualize the testing VS predicted data and check the performance
fig1, ax = plt.subplots()
fig1.suptitle("Visualization of the testing VS the predicted samples")
ax.plot(y_test, label="Testing samples")
ax.plot(pred_ML, label="Predicted samples")
legend1 = ax.legend(shadow=False, fontsize='x-small')
plt.show()


# ~~~~~~~~~~~~~~~~ STEP #3b: Train/Evaluate the DL regression model (LSTM) ~~~~~~~~~~~~~~~~~

# Function that generates data for lstm training
def generate_data(dim):
    # reshape input data
    idx = randint(0, dim)
    y = updated_consumption_data_impute_missing[:, idx]
    x = np.delete(updated_consumption_data_impute_missing, idx, axis=1)
    X = x.reshape(len(x), dim, 1)
    Y = y.reshape(len(y), -1)
    return X, Y, idx


# Define the input dimension and lstm params
dim = x.shape[1]
batch_size = 128
memory_units = 5
epochs = 10
# Define lstm model
model = Sequential()
model.add(LSTM(memory_units, input_shape=(dim, 1)))  # numpy version = 1.19.5
model.add(Dense(1))
model.compile(loss='mean_squared_error', optimizer='adam')
# Fit model for 10 iterations (1 epoch per iteration)
print()
print("*" * 75)
print("\t\t\t\tLSTM MODEL TRAINING:")
print("*" * 75)
print()
for i in range(epochs):
    print("*" * 30 + f" ITERATION {i} " + "*" * 30)
    X, Y, idx = generate_data(dim)
    model.fit(X, Y, epochs=1, batch_size=batch_size, verbose=2)

# Save/Load model - avoid repeating training (time++)
model_path = f"/home/christina/evaluation_test/model/lstm_model_ep{epochs}_un{memory_units}.h5"
model.save(model_path)
# loaded_model = load_model(model_path)

# Generate new data and evaluate the model on them
X_test, Y_test = x.reshape(len(x), dim, 1), y.reshape(len(y), -1)
# Define the number of samples for evaluation
n_samples = len(y_test)
pred_lstm = model.predict(X_test[:n_samples])
for i in range(n_samples):
    print('Expected', Y_test[i, 0], 'Predicted', pred_lstm[i, 0])

print()
print("*" * 75)
print("\t\t\t\tLSTM MODEL EVALUATION:")
print("Mean Squared Error (MSE) = ", mean_squared_error(pred_lstm, Y_test[-n_samples:]))
print("Mean Absolute Error (MAE) = ", mean_absolute_error(pred_lstm, Y_test[-n_samples:]))
print("R2 = ", r2_score(pred_lstm, Y_test[-n_samples:]))
print("*" * 75)
# Visualize the testing VS predicted data and check the performance
fig3, ax = plt.subplots()
fig3.suptitle("Visualization of the testing VS the predicted samples")
ax.plot(Y_test[:n_samples], label="Testing samples")
ax.plot(pred_lstm, label="Predicted samples")
legend3 = ax.legend(shadow=False, fontsize='x-small')
plt.show()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ STEP #4: Predict missing values ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
start = time()
print()
print("*" * 75)
print("\t\t\tFILLING MISSING VALUES WITH PREDICTIONS:")
print("*" * 75)
# Predict missing values with the best regression model (i.e Bayesian)
final_array = copy.deepcopy(array.values)
df = pd.DataFrame(updated_consumption_data_impute_missing)
for i in range(df.shape[1]):
    print(f"Predict missing values in column: {i}/{df.shape[1]}")
    nan_values_per_col = df[consumption_data_impute_missing.iloc[:, i].isnull() == True].values
    nan_values_idx = np.argwhere(np.isnan(consumption_data_impute_missing.iloc[:, i].values)).tolist()
    nan_values_per_col = np.delete(nan_values_per_col, i, axis=1)
    fill_nan_values_per_col = regressor.predict(nan_values_per_col)
    for nan_idx, fill_nan in zip(nan_values_idx, fill_nan_values_per_col):
        final_array[nan_idx[0], i] = fill_nan
end = time()
print("Elapsed time:", end - start)

print()
# Save the prediction and the whole dataset to disk
final_consumption_data = pd.DataFrame(final_array)
final_consumption_data.to_csv(consumption_path.
                              replace(".csv", "-without_NaN-BayesianRegressor.csv"), index=False)
print("All missing values have been filled successfully!")

# ~~~~~~~~~~~~~~~~~~~~~~~~~ STEP #5: Estimate aggregated consumption ~~~~~~~~~~~~~~~~~~~~~~~~~~
# 24 hours - 48 cols per day
# 30 days - 1440 cols per month
# 365 days - 17520 cols per year
aggregated_data = pd.read_csv(consumption_path.
                              replace(".csv", "-without_NaN-BayesianRegressor.csv"))
yearly_consumption, monthly_consumption, daily_consumption = [], [], []
for i in range(aggregated_data.shape[0]):
    yearly_consumption_per_installation = np.sum(aggregated_data.iloc[i, :].values)
    yearly_consumption.append(yearly_consumption_per_installation)
    monthly_consumption_per_installation = yearly_consumption_per_installation / 12
    monthly_consumption.append(monthly_consumption_per_installation)
    daily_consumption_per_installation = yearly_consumption_per_installation / 365
    daily_consumption.append(daily_consumption_per_installation)

# Save the aggregated estimation to dataframe
aggregated_consumption_data = pd.DataFrame(columns={"meter_id"})
aggregated_consumption_data["meter_id"] = consumption_data["meter_id"]
aggregated_consumption_data["daily"] = daily_consumption
aggregated_consumption_data["monthly"] = monthly_consumption
aggregated_consumption_data["yearly"] = yearly_consumption
# Save dataframe to disk
aggregated_consumption_data.to_csv(consumption_path.replace(".csv", "-aggregated.csv"), index=False)
# Estimate the aggregated daily consumption for all installation in dataset
total_daily_consumption_total = np.sum(daily_consumption)
print()
print("*" * 75)
print(f"The aggregated daily consumption for all installations is "
      f"{np.round(total_daily_consumption_total, 3)} kWh")
print("*" * 75)
