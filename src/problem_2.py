# Author: Christina Tzogka
# Created: 17.09.2021

import collections
import copy
import os.path
import pandas as pd
import numpy as np
import seaborn as sns
from time import time
from sklearn.metrics import accuracy_score, classification_report
from sklearn import linear_model
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm
from sklearn.impute import SimpleImputer
from sklearn.metrics import silhouette_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler  # data normalization
from sklearn.cluster import KMeans  # K-means algorithm
import matplotlib.pyplot as plt
import seaborn as sb  # visualization
from termcolor import colored as cl  # text customization

plt.rcParams['figure.figsize'] = (20, 10)
sb.set_style('whitegrid')

# ~~~~~~~~~~~ 2. Problem: Customer Clustering and Classification ~~~~~~~~~~~~~~~~
# A. Clustering
# B. Classification
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Read data
dir = "/home/christina/evaluation_test"
consumption_path = os.path.join(dir, "data/consumption.csv")
consumption_data = pd.read_csv(consumption_path)
aggregated_data = pd.read_csv(consumption_path.replace(".csv", "-aggregated.csv"))
addinfo_path = os.path.join(dir, "data/addInfo.csv")
addinfo_data = pd.read_csv(addinfo_path)
# Check Nan values per column
# print(addinfo_data.isnull().sum())


# ~~~~~~~~~~~~~~~~ STEP #1: Visualize the distribution ~~~~~~~~~~~~~~~~~~~~~~~~ #
print()
print("*" * 75)
print("\t\t\tLOADING DATA & PREPROCESSING...")
print("*" * 75)
# Firstly, impute addInfo selected columns with SimpleImputer
simple_imputer = SimpleImputer(strategy='most_frequent')
num_bedrooms_data = simple_imputer.fit_transform(addinfo_data['num_bedrooms'].values.reshape(-1, 1))
# Map dwelling_type, in order to apply the SimpleImputer on categorical data
# print(addinfo_data['dwelling_type'].unique()) # find all existing types
addinfo_data['dwelling_type'] = addinfo_data['dwelling_type'].map({'semi_detached_house': 0,
                                                                   'terraced_house': 1, 'detached_house': 2,
                                                                   'bungalow': 3, 'flat': 4})
dwelling_type_data = simple_imputer.fit_transform(addinfo_data['dwelling_type'].values.reshape(-1, 1))

# FACT: the installations in the addInfo dataframe are less than the installations
# in the consumption data, thus the intersection should be estimated
installations_A = aggregated_data['meter_id']
installations_B = addinfo_data['meter_id']
intersection = set(installations_A).intersection(installations_B)
installation_idx_A = np.where(np.isin(list(installations_A), list(intersection)))
installation_idx_B = np.where(np.isin(list(installations_B), list(intersection)))

# Store the important variables in a new dataframe
merged_data = pd.DataFrame()
merged_data['yearly'] = aggregated_data['yearly'].loc[installation_idx_A].values
merged_data['monthly'] = aggregated_data['monthly'].loc[installation_idx_A].values
merged_data['daily'] = aggregated_data['daily'].loc[installation_idx_A].values
merged_data['num_bedrooms'] = [num_bedrooms_data[i] for i in installation_idx_B][0]
merged_data['dwelling_type'] = [dwelling_type_data[i] for i in installation_idx_B][0]

print()
print("*" * 75)
print("\t\t\tPREPARING DISTRIBUTIONS...")
print("*" * 75)
# Visualization #1: distribution of the aggregated yearly energy consumption
fig1a = plt.subplots()
sb.distplot(merged_data['yearly'], color='orange', label='yearly')
plt.title('Distribution of the aggregated yearly energy consumption', fontsize=18)
plt.xlabel('energy consumption (kWh)', fontsize=16)
plt.ylabel('frequency', fontsize=16)
plt.xticks(fontsize=14)
plt.savefig('year_distribution.png')
plt.show()

# Visualization #2: distribution of the number of bedrooms
fig1b = plt.subplots()
sb.distplot(merged_data['num_bedrooms'], color='red', label='num_bedrooms')
plt.title('Distribution of the number of bedrooms', fontsize=18)
plt.xlabel('number of bedrooms', fontsize=16)
plt.ylabel('frequency', fontsize=16)
plt.xticks(fontsize=14)
plt.savefig('bedrooms_distribution.png')
plt.show()

# Visualization #3: distribution of the dwelling type
fig1c = plt.subplots()
sb.distplot(merged_data['dwelling_type'], color='purple', label='dwelling_type')
plt.title('Distribution of the dwelling type', fontsize=18)
plt.xlabel('dwelling type', fontsize=16)
plt.ylabel('frequency', fontsize=16)
plt.xticks(fontsize=14)
plt.savefig('dwelling_distribution.png')
plt.show()

# Statistics about the aggregated yearly energy consumption distribution
# print(cl(merged_data['yearly'].describe(), attrs=['bold']))

# ~~~~~~~~~~~~~~~~ STEP #2: Visualize the relation between variables ~~~~~~~~~~~~~~~~ #
# Relation between the aggregated yearly consumption and the num_bedrooms.
fig2 = plt.subplots()
sb.scatterplot('yearly', 'num_bedrooms',
               data=merged_data,
               color='deepskyblue',
               s=150,
               alpha=0.6,
               edgecolor='b')
plt.title('Yearly Consumption / Number of Bedrooms', fontsize=18)
plt.xlabel('yearly_consumption', fontsize=16)
plt.ylabel('num_bedrooms', fontsize=16)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.savefig('yearly_consumption-num_bedrooms.png')
plt.show()

# Relation between the aggregated yearly consumption and the dwelling_type.
fig3 = plt.subplots()
sb.scatterplot('yearly', 'dwelling_type',
               data=merged_data,
               color='green',
               s=150,
               alpha=0.6,
               edgecolor='b')
plt.title('Yearly Consumption / Dwelling type', fontsize=18)
plt.xlabel('yearly_consumption', fontsize=16)
plt.ylabel('dwelling_type', fontsize=16)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.savefig('yearly_consumption-dwelling_type.png')
plt.show()

# Relation between the dwelling_type and the num of bedrooms
fig3b = plt.subplots()
sb.scatterplot('dwelling_type', 'num_bedrooms',
               data=merged_data,
               color='green',
               s=150,
               alpha=0.6,
               edgecolor='b')
plt.title('Dwelling type / Number of Bedrooms', fontsize=18)
plt.xlabel('dwelling_type', fontsize=16)
plt.ylabel('num_bedrooms', fontsize=16)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.savefig('dwelling_type_num_bedrooms.png')
plt.show()

# ~~~~~~~~~~~~~~~~ STEP #3: Normalize data and run k-means ~~~~~~~~~~~~~~~~ #
# Get all data around the same scale
print()
print("*" * 75)
print("\t\t\tCLUSTERING...")
print("*" * 75)
original_data = merged_data.values
original_data = np.nan_to_num(original_data)

# Elbow method indicates the appropriate k-value
sse={}
for k in range(1, 10):
    kmeans = KMeans(n_clusters=k, max_iter=1000).fit(original_data)
    sse[k] = kmeans.inertia_
plt.figure()
plt.plot(list(sse.keys()), list(sse.values()))
plt.xlabel("Number of cluster")
plt.ylabel("Inertia")
plt.show()

# Define basic params and build k-means
clusters = 3
kmeans = KMeans(init='k-means++', n_clusters=clusters)
kmeans.fit(original_data)
cluster_id = kmeans.labels_
# Estimate silhouette score (silhouette ε [-1,1])
silhouette_score = np.round(silhouette_score(original_data, cluster_id, metric='euclidean'), 3)
# print("K-means silhouette score is:", silhouette_score) # 0.598
# Append the cluster id into the merged dataframe
merged_data['cluster_id'] = cluster_id
# Map dwelling type to the initial categories
clustered_data = copy.deepcopy(merged_data)
clustered_data['dwelling_type'] = clustered_data['dwelling_type'] \
    .map({0: 'semi_detached_house', 1: 'terraced_house', 2: 'detached_house', 3: 'bungalow', 4: 'flat'})
clustered_data.to_csv(consumption_path.replace(".csv", "-clustered.csv"), index=False)

# ~~~~~~~~~~~~ STEP #4: Visualize variables relation, including clusters info ~~~~~~~~~~~~~~~ #
# Visualize the customers groups distribution based on the year consumption ανδ num of bedrooms
fig4 = plt.subplots()
sb.scatterplot('yearly', 'num_bedrooms',
               data=clustered_data,
               s=200,
               hue='cluster_id',
               palette='spring',
               alpha=0.5,
               edgecolor='darkgrey')
plt.title('Yearly Consumption / Number of Bedrooms (CLUSTERED)', fontsize=18)
plt.xlabel('yearly_consumption', fontsize=16)
plt.ylabel('num_bedrooms', fontsize=16)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.legend(loc='upper left', fontsize=14)
plt.savefig('cluster_yearly_bedrooms.png')
plt.show()

# Visualize the customers groups distribution based on the year consumption and dwelling type
fig5 = plt.subplots()
sb.scatterplot('yearly', 'dwelling_type',
               data=clustered_data,
               s=200,
               hue='cluster_id',
               palette='spring',
               alpha=0.5,
               edgecolor='darkgrey')
plt.title('Yearly Consumption / Dwelling Type (CLUSTERED)', fontsize=18)
plt.xlabel('yearly_consumption', fontsize=16)
plt.ylabel('dwelling_type', fontsize=16)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.legend(loc='upper left', fontsize=14)
plt.savefig('cluster_yearly_dwelling.png')
plt.show()

# Barplot of the aggregated yearly energy consumption per cluster
avg_consumption_data = clustered_data.groupby(['cluster_id'], as_index=False).mean()
fig6 = plt.subplots()
sns.barplot(x='cluster_id', y='yearly', palette="plasma", data=avg_consumption_data)

# More stats about dwelling_type - num_bedrooms
dwelling_bedroom_stats = pd.DataFrame(
    clustered_data.groupby(['dwelling_type', 'num_bedrooms'])['dwelling_type'].count())
# print(dwelling_bedroom_stats)
dwelling = [l[0] for l in list(dwelling_bedroom_stats.axes[0])]
bedrooms = [l[1] for l in list(dwelling_bedroom_stats.axes[0])]
count = [d[0] for d in dwelling_bedroom_stats.values]
# Barplot of the dwelling_type - num_bedrooms
plt.figure(figsize=(10, 5))
sns.barplot(x=dwelling, hue=bedrooms, y=count)
plt.show()


# ~~~~~~~~~~~~~~~~~~~~~~ STEP #5: Train classifier ~~~~~~~~~~~~~~~~~~~~ #
# cluster_id is supposed to be the target data (y),
# while the rest of columns correspond to the training data (x)
clf_data = copy.deepcopy(merged_data)
y = clf_data['cluster_id'].values
clf_data.drop('cluster_id', axis=1, inplace=True)
x = clf_data.values

# Splitting to training and testing data
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.1)
# Calculate occurrences of each class/cluster in the dataset
print(collections.Counter(y_train))

# ------------------------- STEP #4a: --------------------------- #
print()
print("*" * 75)
print("\t\t\tCLASSIFICATION...")
print("*" * 75)
# Train/Evaluate a classification model (Logistic Regression/KNN/SVM etc)
# clf = LogisticRegression()
# clf = KNeighborsClassifier()
clf = svm.SVC()
clf.fit(x_train, y_train)
start = time()
pred = clf.predict(x_test)
end = time()
elapsed_time = np.round(end - start, 2)
# Estimate accuracy score and generate classification report
accuracy_score(y_test, pred)
target_names = ['cluster 0', 'cluster 1', 'cluster 2']
report = classification_report(y_test, pred, target_names=target_names, output_dict=True)
df_report = pd.DataFrame(report).transpose()
df_report.sort_values(by=['f1-score'], ascending=False)
print(df_report)
print(f"Elapsed classification time: {elapsed_time} sec.")
